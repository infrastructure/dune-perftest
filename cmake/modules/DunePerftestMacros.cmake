# Macros for performance testing

if(NOT dune-perftest_PREFIX)
  # we configure perftest
  set (dune-perftest_PREFIX ${PROJECT_SOURCE_DIR})
endif(NOT dune-perftest_PREFIX)

set (PERFTEST_EXECUTABLE ${dune-perftest_PREFIX}/python/perftest/perftest.py)

macro(create_perftest_conf_file)
  # parse options
  set(options)
  set(oneValueArgs DESTINATION TEMPLATE)
  set(multiValueArgs)
  cmake_parse_arguments(PERFTEST_CONF "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

  # set default values
  if(NOT PERFTEST_CONF_TEMPLATE)
    set(PERFTEST_CONF_TEMPLATE ${dune-perftest_PREFIX}/perftest.conf.in)
    message("create_perftest_conf_file: perftest.conf template not specified")
    message("      using default shipped from perftest: " ${PERFTEST_CONF_TEMPLATE})
    message("      please copy template to your project, adapt and set TEMPLATE parameter")
  endif(NOT PERFTEST_CONF_TEMPLATE)
  if(NOT PERFTEST_CONF_DESTINATION)
    set(PERFTEST_CONF_DESTINATION perftest.conf)
  endif(NOT PERFTEST_CONF_DESTINATION)

  # configure_file(${PROJECT_SOURCE_DIR}/perftest.conf.in ${_outfile})
  configure_file(${PERFTEST_CONF_TEMPLATE} ${PERFTEST_CONF_DESTINATION})

  # A global target that runs all performance testing targets
  if(NOT TESTS STREQUAL "")
    add_custom_target(perftest
      COMMAND ${PERFTEST_EXECUTABLE} --compile --run --conf=${PERFTEST_CONF_DESTINATION} ${TESTS}
    )
  endif(NOT TESTS STREQUAL "")
endmacro()
