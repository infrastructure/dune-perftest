import sqlite3

##
# Returns the results of all the performance measurements so far
#
#    The results are returned from sqlite3.Cursor.fetchall() as a list of sqlite3.Row.
#    The fields in each row can be accessed just like in a dict:
#
#   \code
#      results = get_results('database.db')
#      for result in results:
#        command = result['command']
#   \endcode
#
#    \param dbfile the name of the database file
#    \return results from the database as a list of sqlite3.Row
def get_results(dbfile, program, mode = None):
  con = sqlite3.connect(dbfile)
  con.row_factory = sqlite3.Row
  cursor = con.cursor()

  if mode:
    cursor.execute("SELECT * FROM measurement WHERE program_name = ? AND command_type = ?", [program, mode])
  else:
    cursor.execute("SELECT * FROM measurement WHERE program_name = ?", [program,])

  return cursor.fetchall()

def get_last_results(dbfile, program, modes):
  con = sqlite3.connect(dbfile)
  con.row_factory = sqlite3.Row
  cursor = con.cursor()

  return [cursor.execute("SELECT * FROM measurement WHERE program_name = ? AND command_type = ? ORDER BY start_time DESC", [program, mode]).fetchone() for mode in modes]
