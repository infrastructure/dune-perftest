import sqlite3
import os

## 
# Creates a new SQLite database in \p filename and creates the \p measurement table
# 
# You shouldn't call this function directly, use open_or_create_database() instead. 
# \param filename the location of the new database
def create_database(filename):
  con = sqlite3.connect(filename)
  cursor = con.cursor()

  cursor.execute('''create table measurement (
    _id INTEGER PRIMARY KEY,

    hostname TEXT,
    architecture TEXT,
    system TEXT,
    num_cpus INTEGER,
    processor TEXT,
    ram INTEGER,

    test_run TEXT,
    program_name TEXT,
    revision TEXT,
    command TEXT,
    command_type TEXT,
    start_time INTEGER,
    problem_size INTEGER,
    compiler TEXT,
    compiler_flags TEXT,

    duration REAL,
    returncode INTEGER,
    memory_rss_max REAL,
    memory_rss_avg REAL,
    cpu_percent_max REAL,
    cpu_percent_avg REAL)
  ''')
  
  con.commit()
  cursor.close()

##
# Opens the SQLite database in \p filename, or creates a new one if \p filename doesn't exist. 
#
# \param filename the location of the database file
# \return a connection to the database, as returned by sqlite3.connect()
def open_or_create_database(filename):
  if not os.path.isfile(filename):
    create_database(filename)
  return sqlite3.connect(filename)
