import sqlite3
import os
from .create import open_or_create_database

##
# \brief Reads the log file \p filename and returns its contents as a dict
#
# \param filename the location of the log file with the test results
# \return the contents of the log file
def parse_log_file(filename):
  data = dict()
  with open(filename) as f:
    for line in f.readlines():
      pair = line.split(':')
      if len(pair) == 2:
        key = pair[0].lower()
        value = pair[1].strip()
        data[key] = value
  return data

##
# \brief Stores \p data as a record in database \p db. 
#
# \param data a single measurement result, as returned by parse_log_file()
# \param database the database file name
def save_measurement(data, database):
  con = open_or_create_database(database)
  cursor = con.cursor()

  keys = ', '.join(data.keys())
  placeholders = ', '.join([':%s' % key for key in data.keys()])
  statement = "INSERT INTO measurement (%s) VALUES (%s)" % (keys, placeholders)

  cursor.execute(statement, data)
  con.commit()
  cursor.close()

##
#  Reads the log file \p filename and stores its data into \p database. 
#  If \p remove is \c True, it also deletes the log file. 
#  
#  \param filename the log file name with the test results
#  \param database the database file name
#  \param remove If \c True, the log file \p filename is deleted after reading. 
#                Otherwise, the log file is preserved. 
def save_log(filename, database, remove=False):
  data = parse_log_file(filename)
  save_measurement(data, database)

  if remove:
    os.remove(filename)