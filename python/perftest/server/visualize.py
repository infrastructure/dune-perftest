#!/usr/bin/env python

import cgi
import cgitb
import os
import psycopg2
import psycopg2.extras
import string
from datetime import datetime
from collections import OrderedDict

from common import *
from conf import DATABASE_OPTIONS, ASSETS_DIRECTORY

cgitb.enable()

print("Content-Type: text/html")
print()

def table_row(result, suffix = None):
  if suffix:
    t = load_template('table_row_' + suffix)
  else:
    t = load_template('table_row')
  context = {
    'program_name' : result['program_name'],
    'command' : result['command'],
    'command_type' : result['command_type'],
    'compiler' : result['compiler'],
    'compiler_flags' : result['compiler_flags'],
    'start_time' : datetime.fromtimestamp(result['start_time']).isoformat(' '),
    'computer' : result['hostname'],
    'duration' : format_duration(result['duration']),
    'memory' : format_bytes(int(result['memory_rss_max'])),
    'revision' : result['revision'],
  }
  
  context['row_class'] = ''

  return t.substitute(context)

CSV_COLUMNS = [
  'start_time',
  'duration',
  'memory_rss_max',
  'program_name',
  'command_type',
]

def visualize_test(test, mode):
  con = psycopg2.connect(DATABASE_OPTIONS, cursor_factory=psycopg2.extras.DictCursor)
  cursor = con.cursor()

  modes = get_all_modes(cursor, test)
  tests = get_all_tests(cursor)
  

  if mode:
    t = load_template('results')
    
    cursor.execute("SELECT * FROM measurement WHERE program_name = %s AND command_type = %s;", (test, mode))
    
    csv = ''
    results = cursor.fetchall()
    for result in results:
      csv += '%s, %f, %f\n' % (datetime.fromtimestamp(result['start_time']).isoformat(), result['duration'], result['memory_rss_max'])
    
    print(t.substitute({
      'title' : test,
      'mode' : mode,
      'base_url' : rewrite_query(),
      'directory' : ASSETS_DIRECTORY,
      'results_table' : '\n'.join([table_row(result) for result in results]),
      'data' : csv.replace('\n', '\\n" + \n"'),
      'test_nav' : get_test_navigation(test, mode, tests),
      'mode_nav' : get_mode_navigation(test, mode, modes),
      'main_nav' : get_main_navigation(''),
    }))
    
  else:
    t = load_template('test_overview')
    
    cursor.execute("SELECT * FROM measurement WHERE program_name = %s;", (test,))

    compile_csv = ''
    run_csv = ''
        
    results = cursor.fetchall()
    for result in results:
      if result['command_type'] == 'compile':
        compile_csv += '%s, %f, %f\n' % (datetime.fromtimestamp(result['start_time']).isoformat(), result['duration'], result['memory_rss_max'])
      elif result['command_type'] == 'run':
        run_csv += '%s, %f, %f\n' % (datetime.fromtimestamp(result['start_time']).isoformat(), result['duration'], result['memory_rss_max'])
      
    
    print(t.substitute({
      'title' : test,
      'base_url' : rewrite_query(),
      'directory' : ASSETS_DIRECTORY,
      'results_table' : '\n'.join([table_row(result, 'cr') for result in results]),
      'csv_file_compile' : compile_csv.replace('\n', '\\n" + \n"'),
      'csv_file_run' : run_csv.replace('\n', '\\n" + \n"'),
      'test_nav' : get_test_navigation(test, '', tests),
      'mode_nav' : get_mode_navigation(test, '', modes),
      'main_nav' : get_main_navigation(''),
    }))

  cursor.close()
  con.close()
    

def visualize_run(run):
  t = load_template('run_report')
  
  con = psycopg2.connect(DATABASE_OPTIONS, cursor_factory=psycopg2.extras.DictCursor)
  cursor = con.cursor()
  
  cursor.execute("SELECT program_name, command_type, duration, memory_rss_max FROM measurement WHERE test_run = %s", (run,))
  results = {}
  modes = set()
  for record in cursor:
    test = record['program_name']
    mode = record['command_type']
    modes.add(mode)
    if not test in results:
      results[test] = {}
    for column in ['duration', 'memory_rss_max']:
      results[test][mode + '_' + column] = record[column]
      
  modes = [m for m in modes if m]
  modes.sort()
      
  results_table = '<thead><tr><th>Test name</th>'
  results_table += ''.join(['<th>%s duration</th><th>%s memory</th>' % (get_mode_name(m), get_mode_name(m)) for m in modes])
  results_table += '</tr></thead><tbody>'
  
  for test, values in results.items():
    results_table += '<tr><td><a href="%s">%s</td>' % (rewrite_query(test=test), test)
    results_table += ''.join(['<td>%s</td><td>%s</td>' % (format_duration(values[m + '_duration']), format_bytes(values[m + '_memory_rss_max'])) for m in modes])
    results_table += '</tr>'

  results_table += '</tbody>'
    
  print(t.substitute({
    'run' : run,
    'results_table' : results_table,
    'directory' : ASSETS_DIRECTORY,
    'test_nav' : get_test_navigation('', '', get_all_tests(cursor)),
    'main_nav' : get_main_navigation(''),
  }))
  cursor.close()
  con.close()

def visualize_overview():
  t = load_template('overview')
  con = psycopg2.connect(DATABASE_OPTIONS)
  cursor = con.cursor()
  
  cursor.execute("SELECT DISTINCT program_name FROM measurement;")
  tests = '\n'.join(['<li><a href="%s">%s</a></li>' % (get_url(test=v[0]), v[0])  for v in cursor])
  cursor.execute("SELECT test_run FROM measurement ORDER BY start_time DESC;")
  run_names = list(OrderedDict.fromkeys([v[0] for v in cursor]))
  runs = '\n'.join(['<li><a href="%s">%s</a></li>' % (get_url(run=v), v) for v in run_names if v])
  
  print(t.substitute({
    'directory' : ASSETS_DIRECTORY,
    'base_url' : rewrite_query(),
    'tests' : tests,
    'runs' : runs,
    'test_nav' : get_test_navigation('', '', get_all_tests(cursor)),
    'main_nav' : get_main_navigation('summary'),
  }))
  cursor.close()
  con.close()

form = cgi.FieldStorage()
if 'test' in form:
  test = form.getvalue('test')
  visualize_test(test, form.getvalue('mode'))
elif 'run' in form:
  run = form.getvalue('run')
  visualize_run(run)
else:
  visualize_overview()

