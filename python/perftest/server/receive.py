#!/usr/bin/env python2

import cgi
import cgitb
import os
import base64

from conf import get_spool_dir, check_user

cgitb.enable()

form = cgi.FieldStorage()
auth = os.environ['HTTP_AUTHORIZATION'].split()

authenticated = False
if len(auth) == 2:
  decoded = base64.b64decode(auth[1]).strip().decode('ascii')
  print(decoded)
  if ':' in decoded:
    username, password = decoded.split(':')
    print(username, password)
    if check_user(username, password):
      authenticated = True
      print("Content-Type: text/html")
      print("")
    
      try:
        run = form["run"].value
        if not run:
          print("Run ID must be a valid non-empty string")
          exit()

        dir = os.path.join(get_spool_dir(), run)
        if not os.path.exists(dir):
          os.makedirs(dir)
        
        if form["logfile"].file:
          filename = form["logfile"].filename
        else:
          filename = form["name"].value
        absfilename = os.path.join(dir, filename)

        with open(absfilename, 'w') as f:
          f.write(str(form["logfile"].value))
        
        print("File uplodaded as %s" % absfilename)
      except KeyError as ke:
        print("Field missing: %s" % ke)

if not authenticated:
  print("WWW-Authenticate: Basic")
  print("")
