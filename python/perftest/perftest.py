#!/usr/bin/env python

import argparse

from measure import *
from database import *
from visualize import *
from htmlvisualization import *
from upload import *

import sys
import os
import subprocess
import webbrowser
import uuid

CONFIG_TOP_SECTION = "PERFTEST"
CONFIG_BUILD_SECTION = "BUILD"
CONFIG_MEASURE_SECTION = "MEASURE"
KEY_COMMAND_TYPE = "CommandType"
KEY_OUTPUT = "OutputFileName"
KEY_BUILDER = "Builder"
KEY_VCS = "VersionControl"
KEY_UPLOAD = "Upload"

try:
  import configparser
except ImportError:
  import ConfigParser as configparser

##
# \brief Performs performance measurements on \p tests.
#
# This function measures the performance of \p tests,
# stores the data in log files, saves it to a database,
# and visualizes it in HTML files.
#
# \param tests the list of test names to run
# \param config_file an optional configuration file
# \param compile boolean specifying whether compile-time should be measured
# \param run boolean specifying whether run-time should be measured
# \param server if specified, all log files are uploaded to the server.
#
def test_project(tests, config_file, compile = True, run = True, server = ''):
  dir_name = os.path.dirname(os.path.abspath(config_file))
  output_dir = os.path.join(dir_name, 'perftest_output')
  db_file = os.path.join(output_dir, os.path.basename(config_file).replace('.conf', '.db'))

  if not os.path.exists(output_dir):
    os.makedirs(output_dir)

  extra_data = {}
  build_config = {}
  measure_config = {}
  do_upload = False

  # we do not check for the config file, we require the file to be present!
  config = configparser.ConfigParser()
  read_file = config.read(config_file)
  assert(read_file == [config_file])

  if not CONFIG_TOP_SECTION in config.sections():
    raise SyntaxError("The configuration file must have a [%s] section"%CONFIG_TOP_SECTION)

  if config.has_option(CONFIG_TOP_SECTION, KEY_VCS):
    vcs = config.get(CONFIG_TOP_SECTION,KEY_VCS)
    srcdir_name = config.get(CONFIG_TOP_SECTION, "SourceDir")
    extra_data['revision'] = get_revision(vcs, srcdir_name)
  do_upload = config.getboolean(CONFIG_TOP_SECTION,KEY_UPLOAD,fallback=False)

  build_config = {k:v for k,v in config.items(CONFIG_BUILD_SECTION)}
  measure_config = {k:v for k,v in config.items(CONFIG_MEASURE_SECTION)}

  run_name = uuid.uuid1().hex
  extra_data['test_run'] = run_name
  extra_data.update(computer_parameters())

  modes = {}
  if compile and run:
    modes['cr'] = "Overview"
  if compile:
    modes['compile'] = "Compile"
  if run:
    modes['run'] = "Run"

  logfiles = []

  for test in tests:
    name = os.path.basename(test)
    if compile:
      c = measure_compile(test, build_config)
      c.update(extra_data)
      log = os.path.join(output_dir, name + "_compile.log")
      write_log_file(log, c)
      logfiles.append(log)
      save_log(os.path.join(output_dir, name + "_compile.log"), db_file)

    if run:
      r = measure_run(test, measure_config)
      r.update(extra_data)
      log = os.path.join(output_dir, name + "_run.log")
      write_log_file(log, r)
      logfiles.append(log)
      save_log(os.path.join(output_dir, name + "_run.log"), db_file)

  html = HtmlVisualization(output_dir, tests, modes, db_file)
  html.generate_all()
  html.generate_summary()
  # webbrowser.open(os.path.join(output_dir, 'index.html'))

  if do_upload:
    for log in logfiles:
      upload_file(log, server, run_name, dir_name)

## \cond
if __name__ == "__main__":
  print(sys.argv)

  parser = argparse.ArgumentParser(description='DUNE Performance Testing')
  parser.add_argument('--conf', default='perftest.conf')

  parser.add_argument('-c', '--compile', action='store_true')
  parser.add_argument('-r', '--run', action='store_true')

  parser.add_argument('tests', type=str, nargs='+', help='List of performance tests to run')
  parser.add_argument('--server', type=str, default="http://localhost/cgi-bin/receive.py")

  args = parser.parse_args()
  print(args)

  test_project(args.tests, args.conf, args.compile, args.run, args.server)

## \endcond
