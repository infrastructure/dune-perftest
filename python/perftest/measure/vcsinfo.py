import subprocess

## \cond

def get_revision_git(directory):
  try:
    output = subprocess.check_output(["git", "rev-parse", "--short", "HEAD"], cwd=directory)
    return output.decode('utf8').strip()
  except subprocess.CalledProcessError:
    return ''

def get_revision_svn(directory):
  try:
    output = subprocess.check_output(["svnversion"], cwd=directory)
    return output.decode('utf8').strip()
  except subprocess.CalledProcessError:
    return ''

## \endcond

##
# \brief Determines the current working directory revision.
#
# Currently, only Git and Subversion are supported.
# \param directory the directory with the source code
# \param vcs version control system used, either \c git or \c svn
# \return the revision number (for Subversion) or hash (for Git)
def get_revision(vcs, directory):
  return eval("get_revision_%s"%vcs)(directory)
