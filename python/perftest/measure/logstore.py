import subprocess

##
# Writes performance testing results \p values into \p logfile
# \param logfile output log file name
# \param values dict of testing results, as returned by measure()
#
def write_log_file(logfile, values):
  with open(logfile, 'w') as f:
    for key, value in values.items():
      f.write("%s: %s\n" % (key.upper(), value))
