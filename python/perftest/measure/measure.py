import subprocess
import psutil
import time
import platform
import os

##
# Specifies the interval between consecutive memory consumption measurements in seconds
MEMORY_MEASURE_INTERVAL = 0.0002

##
# \brief Returns the average and maximal values from the list \p values.
#
#    They are returned as a dict with the keys starting with \p name
#    and having '_avg' and '_max' appended, respectively.
#
#    \param name the name of the quantity. This is used to contruct keys of the returned dict
#    \param values a list of numbers. This functions calls max(), sum() and len() on it.
def average_and_max(name, values):
  return {
    name + '_max' : max(values),
    name + '_avg' : sum(values) / float(len(values)),
  }

##
# \brief Returns the parameters of the computer it is run on.
#
#    This function uses data from \c platform and \c psutil Python packages.
#    The returned dict contains the following keys:
#
#    * \c hostname: The computer's host name
#    * \c architecture: The computer's architecture, e.g. i386 or x86_64
#    * \c system: The operating system
#    * \c num_cpus: The number of processors
#    * \c processor: The actual processor's name, may be empty on some platforms
#    * \c ram: The total amount of RAM the computer has, may be zero on some platforms
#
#    \return a dict with the keys listed above
def computer_parameters():

  p = dict()

  p['hostname'] = platform.node()
  p['architecture'] = platform.machine()
  p['system'] = platform.system()

  p['num_cpus'] = psutil.cpu_count()
  p['processor'] = platform.processor()
  try:
    p['ram'] = psutil.virtual_memory()[0]
  except AttributeError:
    p['ram'] = 0

  return p

##
# Measures the running time and memory consumption of \p command.
#
#  Optional arguments \p command_type, \p problem_size and \p compiler are added to the returned dict.
#  They can be useful for comparisons and algorithmic complexity calculation.
#
#  \param program_name the human-readable name of the program
#  \param command the command to run, can be a string or a list of strings to represent arguments
#  \param command_type optional command type string, can be \c run, \c compile or anything else.
#
#  \return a dict of performance stats for running \p command
def measure(program_name, config, command, command_type = 'run'):
  if type(command) is not list:
    command = [command]
  engine = config['engine']
  return eval("measure_%s"%engine)(program_name, config, command, command_type)

def measure_psutil(program_name, config, command, command_type):
  print("Measuring command: ", command)

  p = subprocess.Popen(command, stderr=subprocess.PIPE)
  ps = psutil.Process(p.pid)

  memory = []
  cpu_percent = []
  start_time = time.time()

  while p.poll() is None:
    memory.append(ps.memory_info()[0])
    cpu_percent.append(ps.cpu_percent())
    time.sleep(MEMORY_MEASURE_INTERVAL)

  end_time = time.time()

  results = {
    'program_name' : program_name,
    'start_time' : int(start_time),
    'command' : command,
    'command_type' : command_type,
    'duration' : end_time - start_time,
    'returncode' : p.returncode,
  }

  results.update(average_and_max('memory_rss', memory))
  results.update(average_and_max('cpu_percent', cpu_percent))

  return results

def measure_foobar(program_name, config, command, command_type):
  results = {
    'program_name' : program_name,
    'start_time' : 666,
    'command' : command,
    'command_type' : command_type,
    'duration' : config['foobar_timing'],
    'returncode' : 0,
    'memory_rss_avg' : 12345,
    'memory_rss_max' : 54321,
    'cpu_percent_avg' : 0.75,
    'cpu_percent_max' : 0.9,
  }

  return results

def measure_pacxx(program_name, config, command, command_type):
  profile_config = config['pacxx_cfg']
  # set environment variables
  pacxx_env = {
    'PACXX_PROF_ENABLE' : '1'
  }
  pacxx_env.update(os.environ)
  # run program
  subprocess.Popen(command, stderr=subprocess.PIPE, env=pacxx_env)
  # parse json file
  # update results
  results = {
    'program_name' : program_name,
    'start_time' : 666,
    'command' : command,
    'command_type' : command_type,
    'duration' : 2,
    'returncode' : 0,
    'memory_rss_avg' : 12345,
    'memory_rss_max' : 54321,
    'cpu_percent_avg' : 0.75,
    'cpu_percent_max' : 0.9,
  }

  return results

##
# \brief Measure the compile-time resources consumption of \p test.
#
# \param test the name of the test to run
# \param make_exe make executable used for compilation
# \return a dict of the performance stats for compiling \p test
def measure_compile(test, build_config):
  engine = build_config['builder']
  return eval("measure_compile_%s"%engine)(test, build_config)

def measure_compile_default(test, clean_cmd, build_cmd):
  subprocess.call(clean_cmd)
  psutil_config = {}
  return measure_psutil(test, psutil_config, build_cmd, command_type='compile')

def measure_compile_cmake(test, build_config):
  directory = os.path.abspath(os.path.dirname(test))
  target = os.path.basename(test)
  exe = build_config['cmake']
  assert(exe != '')
  clean_cmd = [exe, '--build', directory, '--target', 'clean']
  build_cmd = [exe, '--build', directory, '--target', target]
  return measure_compile_default(test, clean_cmd, build_cmd)

def measure_compile_make(test, build_config):
  directory = os.path.abspath(os.path.dirname(test))
  target = os.path.basename(test)
  exe = build_config['make']
  assert(exe != '')
  clean_cmd = [exe, '-C', directory, 'clean']
  build_cmd = [exe, '-C', directory, target]
  return measure_compile_default(test, clean_cmd, build_cmd)

##
# \brief Measure the run-time resources consumption of \p test.
#
# \param test the name of the test to run
# \return a dict of the performance stats for running \p test
def measure_run(test, config):
  abs_test = os.path.abspath(test)
  return measure(test, config, abs_test, command_type='run')
