from numpy import array

## The values is not an outlier
NOT_OUTLIER = 0

## The value is at least one standard deviation lower than mean
OUTLIER_LOW = 1

## The value is at least three standard deviations lower than mean
OUTLIER_VERY_LOW = 3

## The value is at least one standard deviation higher than mean
OUTLIER_HIGH = 4

## The value is at least three standard deviations higher than mean
OUTLIER_VERY_HIGH = 5

##
# \brief Checkes whether \p value is an outlier
#
# This function returns the outlier type of \p value
# in a distribution with mean \p mean and standard deviation \p dev. 
# It distinguishes between very high and very low values (over 3 sigma), 
# high and low values (between 1 and 3 sigma) and normal values. 
#
# \param value the value to check
# \param mean the distribution's mean
# \param dev the distribution's standard deviation
# \return the outlier type, as one of the constants from this file
def get_outlier_type(value, mean, dev):
  return NOT_OUTLIER if abs(value - mean) < dev else (OUTLIER_HIGH if value > mean else OUTLIER_LOW) if abs(value-mean) < 3*dev else (OUTLIER_VERY_HIGH if value > mean else OUTLIER_VERY_LOW)

##   
#   \brief Finds outliers in \p data
#    
#   Outliers are points that deviate from the mean for at least \p num_sigma times the standard deviation. 
#   
#   \param data the data as a list of numbers, will be converted to a numpy array
#   \param num_sigma criterion for outliers, higher values mean fewer outliers
#   \return the outlier types, as a list of constants from this file
def find_outliers(data, num_sigma = 1):
  """
  """
  a = array(data)
  mean = a.mean()
  dev = a.std() * num_sigma
  
  print("Finding outliers:", mean, dev)
  
  outliers = [get_outlier_type(i, mean, dev) for i in a]
  return outliers

## \cond
if __name__ == "__main__":
  data = [1, 5, 5, 5, 5, 5, 5, 5, 5, 20]
  print(find_outliers(data, 1))
  print(find_outliers(data, 2))
  print(find_outliers(data, 3))
  
## \endcond
