
add_executable("dune_perftest" dune_perftest.cc)
target_link_dune_default_libraries("dune_perftest")

add_executable("dune_perftest_matrix" matrix.cc)
target_link_dune_default_libraries("dune_perftest_matrix")

